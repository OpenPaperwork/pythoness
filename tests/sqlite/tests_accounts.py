import unittest

import openpaperwork_core


class TestDb(unittest.TestCase):
    def setUp(self):
        self.core = openpaperwork_core.Core(auto_load_dependencies=True)
        self.core.load("pythoness.model.accounts")
        self.core.load("pythoness.sqlite.accounts")
        self.core.init()

        (self.db_url, fd) = self.core.call_success(
            "fs_mktemp", prefix="pythoness_tests_", on_disk=True
        )
        fd.close()
        self.core.call_all("db_open", self.db_url)

    def tearDown(self):
        self.core.call_all("fs_unlink", self.db_url)

    def test_add_upd_del(self):
        accounts = []
        self.core.call_success("db_account_get_all", accounts)
        self.assertEqual(accounts, [])

        account = self.core.call_success("account_new", "test", "test")
        self.core.call_all("db_account_add", account)

        accounts = []
        self.core.call_success("db_account_get_all", accounts)
        self.assertEqual(accounts, [account])

        account.label = "something else"
        accounts = self.core.call_success("db_account_update", account)

        accounts = []
        self.core.call_success("db_account_get_all", accounts)
        self.assertEqual(accounts, [account])

        self.core.call_success("db_account_delete", account)
        accounts = []
        self.core.call_success("db_account_get_all", accounts)
        self.assertEqual(accounts, [])
