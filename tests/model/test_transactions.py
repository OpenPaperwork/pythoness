#!/usr/bin/env python3

import datetime
import unittest

import openpaperwork_core


generated = 0


def gen_id():
    global generated
    generated += 1
    return str(generated)


class TransactionTestCase(unittest.TestCase):
    def setUp(self):
        self.core = openpaperwork_core.Core(auto_load_dependencies=True)
        self.core.load("pythoness.model.accounts")
        self.core.load("pythoness.model.transactions")
        self.core.init()

        self.dumb_account = self.core.call_success(
            "account_new", "DUMB_ACCOUNT", "DUMB_ACCOUNT"
        )
        self.dumb_account_b = self.core.call_success(
            "account_new", "DUMB_ACCOUNT_B", "DUMB_ACCOUNT_B"
        )

    def test_denominator(self):
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="PAYPAL 0888 999 888",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "PAYPAL"
        )
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="AMAZON PAYMENTS PARIS9999999/",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "AMAZON PAYMENTS"
        )
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="SNCF INTERNET PARIS CEDEX 99",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "SNCF INTERNET"
        )
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="PUM MULHOUSE",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "PUM MULHOUSE"
        )
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="PUM",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "PUM"
        )
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="APRR AUTOROUTE 99999",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "APRR AUTOROUTE"
        )
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "SPOTIFY STOCKHOLM"
        )
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="SPOTIFY P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "SPOTIFY STOCKHOLM"
        )
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="SARL BROCHIN SOMEWHERE",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "SARL BROCHIN"
        )
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="STEAMGAMES 999-999-99 99,99 EUR",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "STEAMGAMES EUR"
        )
        self.assertEqual(
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="STEAMGAMES.COM 4 HAMBURG",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ).denominator,
            "STEAMGAMES COM"
        )

    def test_sort(self):
        t1 = self.core.call_success(
            "transaction_new",
            account=self.dumb_account,
            transaction_id=gen_id(),
            label="Spotify P999999 Stockholm",
            vdate=datetime.datetime(year=2018, month=1, day=2),
            amount=-10.0,
        )
        t2 = self.core.call_success(
            "transaction_new",
            account=self.dumb_account,
            transaction_id=gen_id(),
            label="SPOTIFY P999999 Stockholm",
            vdate=datetime.datetime(year=2018, month=1, day=1),
            amount=-10.0,
        )
        a = [t1, t2]
        a.sort()
        self.assertEqual(a, [t2, t1])


class TransactionGroupTestCase(unittest.TestCase):
    def setUp(self):
        self.core = openpaperwork_core.Core(auto_load_dependencies=True)
        self.core.load("pythoness.model.accounts")
        self.core.load("pythoness.model.transactions")
        self.core.load("pythoness.model.transaction_groups")
        self.core.init()

        self.dumb_account = self.core.call_success(
            "account_new", "DUMB_ACCOUNT", "DUMB_ACCOUNT"
        )
        self.dumb_account_b = self.core.call_success(
            "account_new", "DUMB_ACCOUNT_B", "DUMB_ACCOUNT_B"
        )

    def test_sort(self):
        g1 = self.core.call_success(
            "transaction_group_new", [
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="Spotify P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=2),
                    amount=-10.0,
                ),
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="SPOTIFY P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=1),
                    amount=-10.0,
                ),
            ]
        )
        g2 = self.core.call_success(
            "transaction_group_new", [
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="PUM MULHOUSE",
                    vdate=datetime.datetime(year=2018, month=1, day=1),
                    amount=-10.0,
                ),
            ]
        )
        a = [g1, g2]
        a.sort()
        self.assertEqual(a, [g2, g1])

    def test_mean(self):
        g1 = self.core.call_success(
            "transaction_group_new", [
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="Spotify P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=3),
                    amount=-30.0,
                ),
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="SPOTIFY P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=1),
                    amount=-120.0,
                ),
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="SPOTIFY P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=5),
                    amount=-90.0,
                ),
            ]
        )
        self.assertEqual(g1.mean_amount, -80.0)
        self.assertEqual(g1.mean_day_interval, 2)

    def test_mean_per_day(self):
        g1 = self.core.call_success(
            "transaction_group_new", [
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="SPOTIFY P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=1),
                    amount=-60.0,
                ),
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="Spotify P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=3),
                    amount=-60.0,
                ),
            ]
        )
        self.assertEqual(g1.mean_amount_per_day, -60)

        g1 = self.core.call_success(
            "transaction_group_new",  [
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="SPOTIFY P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=1),
                    amount=-60.0,
                ),
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="Spotify P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=3),
                    amount=-60.0,
                ),
            ],
            start_vdate=datetime.datetime(year=2018, month=1, day=1),
            end_vdate=datetime.datetime(year=2018, month=1, day=5),
        )
        self.assertEqual(g1.mean_amount_per_day, -30)

        g1 = self.core.call_success(
            "transaction_group_new", [
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="SPOTIFY P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=1),
                    amount=-60.0,
                ),
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="Spotify P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=3),
                    amount=-60.0,
                ),
                self.core.call_success(
                    "transaction_new",
                    account=self.dumb_account,
                    transaction_id=gen_id(),
                    label="Spotify P999999 Stockholm",
                    vdate=datetime.datetime(year=2018, month=1, day=5),
                    amount=-60.0,
                ),
            ]
        )
        self.assertEqual(g1.mean_amount_per_day, -45)


class TransactionGroupingTestCase(unittest.TestCase):
    def setUp(self):
        self.core = openpaperwork_core.Core(auto_load_dependencies=True)
        self.core.load("pythoness.model.accounts")
        self.core.load("pythoness.model.transactions")
        self.core.load("pythoness.model.transaction_groups")
        self.core.init()

        self.dumb_account = self.core.call_success(
            "account_new", "DUMB_ACCOUNT", "DUMB_ACCOUNT"
        )
        self.dumb_account_b = self.core.call_success(
            "account_new", "DUMB_ACCOUNT_B", "DUMB_ACCOUNT_B"
        )

    def test_group_transactions(self):
        transactions = [
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=3),
                amount=-30.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="SPOTIFY P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-120.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="AMAZON PAYMENTS PARIS9999999/",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-20.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="AMAZON PAYMENTS PARIS9999999/",
                vdate=datetime.datetime(year=2018, month=1, day=5),
                amount=-10.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="SNCF INTERNET PARIS CEDEX 99",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
        ]
        groups = []
        self.core.call_success("transaction_group_all", groups, transactions)
        self.assertEqual(groups[0].denominator, "AMAZON PAYMENTS")
        self.assertEqual(len(groups[0].transactions), 2)
        self.assertEqual(groups[1].denominator, "SPOTIFY STOCKHOLM")
        self.assertEqual(len(groups[1].transactions), 2)

    def test_different_accounts(self):
        transactions = [
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=3),
                amount=-30.0,
            ),
            self.core.call_success(
                "transaction_new",
                account=self.dumb_account_b,
                transaction_id=gen_id(),
                label="SPOTIFY P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-120.0,
            ),
        ]
        groups = []
        self.core.call_success("transaction_group_all", groups, transactions)
        self.assertEqual(len(groups), 0)


class TestTransactionDistance(unittest.TestCase):
    def setUp(self):
        self.core = openpaperwork_core.Core(auto_load_dependencies=True)
        self.core.load("pythoness.model.accounts")
        self.core.load("pythoness.model.transactions")
        self.core.init()

        self.dumb_account = self.core.call_success(
            "account_new", "DUMB_ACCOUNT", "DUMB_ACCOUNT"
        )
        self.dumb_account_b = self.core.call_success(
            "account_new", "DUMB_ACCOUNT_B", "DUMB_ACCOUNT_B"
        )

    def test_distance_zero(self):
        t1 = self.core.call_success(
            "transaction_new",
            account=self.dumb_account,
            transaction_id=gen_id(),
            label="Spotify P999999 Stockholm",
            vdate=datetime.datetime(year=2018, month=1, day=1),
            amount=-10.0,
        )
        t2 = self.core.call_success(
            "transaction_new",
            account=self.dumb_account,
            transaction_id=gen_id(),
            label="SPOTIFY P999999 Stockholm",
            vdate=datetime.datetime(year=2018, month=1, day=1),
            amount=-10.0,
        )
        self.assertEqual(t1.distance(t2, 20, 100), 0)

    def test_distance_date(self):
        t1 = self.core.call_success(
            "transaction_new",
            account=self.dumb_account,
            transaction_id=gen_id(),
            label="Spotify P999999 Stockholm",
            vdate=datetime.datetime(year=2018, month=1, day=1),
            amount=-10.0,
        )
        t2 = self.core.call_success(
            "transaction_new",
            account=self.dumb_account,
            transaction_id=gen_id(),
            label="SPOTIFY P999999 Stockholm",
            vdate=datetime.datetime(year=2018, month=1, day=2),
            amount=-10.0,
        )
        self.assertEqual(t1.distance(t2, 10, 100), 0.1)

    def test_distance_amount(self):
        t1 = self.core.call_success(
            "transaction_new",
            account=self.dumb_account,
            transaction_id=gen_id(),
            label="Spotify P999999 Stockholm",
            vdate=datetime.datetime(year=2018, month=1, day=1),
            amount=-1000.0,
        )
        t2 = self.core.call_success(
            "transaction_new",
            account=self.dumb_account,
            transaction_id=gen_id(),
            label="SPOTIFY P999999 Stockholm",
            vdate=datetime.datetime(year=2018, month=1, day=1),
            amount=-900.0,
        )
        self.assertEqual(t2.distance(t1, 10, -1000), 0.1)
