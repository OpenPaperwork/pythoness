# Pythoness

Predictive personal finance application


## End goal

Most personal finance applications seem to focus on the past. They can tell
you precisely where you were, but they can barely give you any idea of where
you're going. Also most of them require you to input a huge amount of data,
manually (<<< loosing your soul and mind here).

Unlike them, Pythoness has only two goals:
1. Tell you whether you're going to fly or crash.
2. Let you be lazy (crushing numbers is a machine's job).

You shouldn't expect anything more from Pythoness. Any other feature is at best
a side-effect to reach those goals.


## Status

- Barely working
- Experimental


## Main features

- Automatically import bank statements using [Web-OOB](https://weboob.org/)
- Makes wild guesses regarding your future based on those statements
- GTK GUI (none of this web applications crap)


## What does it look like ?

![crap, but meh, it's still experimental, so I'm not going to waste my time making something beautiful yet](screenshot.png)

^ Beware of my mad [Gimp](https://www.gimp.org/) skills!!


## How does it work ?

It looks for recurrent transactions in your bank statements, and simply
average the remaining transactions. From there, taking a guess at your
financial future is trivial.


### Recurrent transactions

Pythoness examines your bank statements and looks for recurrent transactions.
To figure out if transactions are recurrent, it needs at least 3 points each
time.

For instance, for monthly transactions with a description "TOTO", N being the
current month, it needs the transactions "TOTO" of months N-1, N-2, and N-3
(or N, N-1, N-2). The 2 first points are used to emit an hypothesis (called
"predictions"), and the 3rd point to validate it. The more points the better.

In other words, for monthly transactions, you need at least 3 months of
bank statements for this algorithm to work.


### Non-recurrent transactions

Transactions that are not detected as recurrent are simply averaged.
Pythoness computes the average per day for each of your account.


## What if Web-OOB can't download statements for my accounts ?

Web-OOB doesn't support all the banks in the world. If yours isn't supported,
you can either send a patch to Web-OOB or you can input them manually
(in both cases, welcome to a world of pain).

For some types of accounts, the word "transaction" has no meaning or they are
not really helpful. In that case, you can simply input your account balances
every months manually and let Pythoness compute an average.


## Is detecting recurrent transactions really useful ?

Recurrent transactions detection is not required to know if you're going to
fly or crash on the long run. Averages are enough.

However, if you want to know if you're going to fly or crash in the short term,
you need predictions to be as precise as possible. For example, you can think
of a student wondering whether they are going to be above or below 0 at the
end of the month.


## Does it run on Windows or MacOSX?

Hell no ! Go away [freedom hater](https://www.fsf.org/about/what-is-free-software) !


## Installation / development environment

```sh
git clone https://framagit.org/OpenPaperwork/pythoness.git
cd pythoness

source ./activate_test_env.sh
make install
pythoness chkdeps
```

## Running

```sh
source ./activate_test_env.sh
pythoness
```


## License

AGPL v3+
