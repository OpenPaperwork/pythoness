import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = 900

    def get_interfaces(self):
        return [
            'account_storage',
            'db_open',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'db',
                'defaults': ['pythoness.sqlite.db'],
            },
        ]

    def db_open(self, file_url):
        db = self.core.call_success("db_get_cursor")
        self.core.call_success(
            "db_execute", db,
            "CREATE TABLE IF NOT EXISTS accounts ("
            " id TEXT UNIQUE NOT NULL,"
            " label TEXT UNIQUE NOT NULL,"
            " balance INTEGER NOT NULL"  # thousands of euros
            ")"
        )
        db.close()

    def db_account_get_all(self, out: list):
        db = self.core.call_success("db_get_cursor")
        if db is None:
            return True

        accounts = self.core.call_success(
            "db_execute", db, "SELECT id, label, balance FROM accounts"
        )
        out += [
            self.core.call_success(
                "account_new",
                account[0],
                account[1],
                account[2] / 1000
            )
            for account in accounts
        ]
        db.close()
        out.sort(key=lambda account: account.label.lower())
        return True

    def db_account_get(self, account_id):
        db = self.core.call_success("db_get_cursor")
        if db is None:
            return True

        accounts = self.core.call_success(
            "db_execute", db,
            "SELECT id, label, balance FROM accounts"
            " WHERE id = ? LIMIT 1",
            (account_id,)
        )
        accounts = list(accounts)
        account = accounts[0]
        account = self.core.call_success(
            "account_new",
            account[0],
            account[1],
            account[2] / 1000
        )
        db.close()
        return account

    def db_account_add(self, account):
        LOGGER.info("Adding/updating account %s", account)
        db = self.core.call_success("db_get_cursor")

        r = self.core.call_success(
            "db_execute", db,
            "SELECT rowid FROM accounts WHERE id = ? LIMIT 1;",
            (account.account_id,)
        )
        r = list(r)
        if len(r) > 0:
            self.core.call_success(
                "db_execute", db,
                "UPDATE accounts SET balance = ?"
                " WHERE id = ?",
                (
                    int(account.current_balance * 1000),
                    account.account_id
                )
            )
            self.core.call_success("db_execute", db, "COMMIT")
            return

        self.core.call_success(
            "db_execute", db,
            "INSERT INTO accounts (id, label, balance) VALUES (?, ?, ?)",
            (
                account.account_id, account.label,
                int(account.current_balance * 1000)
            )
        )
        self.core.call_success("db_execute", db, "COMMIT")
        db.close()
        self.core.call_all("on_db_modified")

    def db_account_update(self, account):
        db = self.core.call_success("db_get_cursor")
        if self.core.call_success("db_transactions_has", account) is None:
            LOGGER.info(
                "No transactions left on the account '%s'"
                " => reseting its balance", account.account_id
            )
            account.current_balance = 0
        self.core.call_success(
            "db_execute", db,
            "UPDATE accounts SET label = ?, balance = ?"
            " WHERE id = ?",
            (
                account.label, int(account.current_balance * 1000),
                account.account_id
            )
        )
        self.core.call_success("db_execute", db, "COMMIT")
        db.close()
        self.core.call_all("on_db_modified")

    def db_account_delete(self, account):
        db = self.core.call_success("db_get_cursor")
        self.core.call_success(
            "db_execute", db,
            "DELETE FROM accounts WHERE id = ?",
            (account.account_id,)
        )
        self.core.call_success("db_execute", db, "COMMIT")
        db.close()
        self.core.call_all("on_db_modified")
