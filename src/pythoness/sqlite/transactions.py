import datetime
import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = 875

    def get_interfaces(self):
        return [
            'db_open',
            'transaction_storage'
        ]

    def get_deps(self):
        return [
            {
                'interface': 'account_storage',
                'defaults': ['pythoness.sqlite.accounts'],
            },
            {
                'interface': 'db',
                'defaults': ['pythoness.sqlite.db'],
            },
        ]

    def db_open(self, file_url):
        db = self.core.call_success("db_get_cursor")
        self.core.call_success(
            "db_execute", db,
            "CREATE TABLE IF NOT EXISTS transactions ("
            " account INTEGER NOT NULL,"
            # transaction_id is used to avoid double-imports ; fall back on
            # rowid if NULL
            " id TEXT UNIQUE NULL,"
            " label TEXT NOT NULL,"
            " vdate INTEGER NOT NULL,"  # Unix timestamp
            " amount INTEGER NOT NULL,"  # thousands of units
            " FOREIGN KEY(account) REFERENCES accounts(rowid)"
            " ON DELETE CASCADE"
            ")"
        )
        db.close()

    @staticmethod
    def _convert_dates_to_timestamps(start_date, end_date):
        if start_date is None:
            start_date = 0
        else:
            start_date = int(start_date.timestamp())
        if end_date is None:
            end_date = datetime.datetime(year=3000, month=1, day=1)
        end_date = int(end_date.timestamp())
        return (start_date, end_date)

    def db_transaction_get(self, transaction_id):
        db = self.core.call_success("db_get_cursor")
        transactions = list(self.core.call_success(
            "db_execute", db,
            "SELECT "
            " accounts.id, accounts.label,"
            " transactions.id, transactions.label, transactions.vdate,"
            " transactions.amount"
            " FROM transactions"
            " INNER JOIN accounts ON accounts.rowid = transactions.account"
            " WHERE transactions.id = ?"
            " LIMIT 1;",
            (transaction_id,)
        ))
        transaction = transactions[0]
        account = self.core.call_success(
            "account_new", transaction[0], transaction[1]
        )
        transaction = self.core.call_success(
            "transaction_new",
            account=account,
            transaction_id=transaction[2],
            label=transaction[3],
            vdate=datetime.datetime.fromtimestamp(transaction[4]),
            amount=transaction[5] / 1000
        )
        db.close()
        return transaction

    def _get(self, out: list, where="", *args, **kwargs):
        db = self.core.call_success("db_get_cursor")
        if db is None:
            return None
        transactions = list(self.core.call_success(
            "db_execute", db,
            "SELECT "
            " accounts.id, accounts.label,"
            " transactions.id, transactions.label, transactions.vdate,"
            " transactions.amount"
            " FROM transactions"
            " INNER JOIN accounts ON accounts.rowid = transactions.account"
            " " + where +
            " ORDER BY vdate;",
            *args, **kwargs
        ))

        # make sure we instantiate accounts only once
        accounts = {}
        for transaction in transactions:
            if transaction[0] not in accounts:
                accounts[transaction[0]] = self.core.call_success(
                    "account_new", transaction[0], transaction[1]
                )

        out += [
            self.core.call_success(
                "transaction_new",
                account=accounts[transaction[0]],
                transaction_id=transaction[2],
                label=transaction[3],
                vdate=datetime.datetime.fromtimestamp(transaction[4]),
                amount=transaction[5] / 1000
            )
            for transaction in transactions
        ]
        db.close()
        return True

    def db_transactions_get(self, out: list, start_date=None, end_date=None):
        (start_date, end_date) = self._convert_dates_to_timestamps(
            start_date, end_date
        )
        return self._get(
            out,
            "WHERE transactions.vdate >= ? AND transactions.vdate <= ?",
            (start_date, end_date)
        )

    def db_transactions_get_by_account(
            self, out: list, account, start_date=None, end_date=None):
        (start_date, end_date) = self._convert_dates_to_timestamps(
            start_date, end_date
        )
        return self._get(
            out,
            "WHERE accounts.id = ?"
            " AND transactions.vdate >= ?"
            " AND transactions.vdate <= ?",
            (account.account_id, start_date, end_date)
        )

    def db_transactions_get_first_vdate(self):
        db = self.core.call_success("db_get_cursor")
        if db is None:
            return None

        r = self.core.call_success(
            "db_execute", db,
            "SELECT vdate FROM transactions ORDER BY vdate LIMIT 1"
        )
        r = list(r)
        if len(r) <= 0:
            return None
        return datetime.datetime.fromtimestamp(r[0][0])

    def db_transactions_add(self, transactions):
        db = self.core.call_success("db_get_cursor")

        accounts = {}
        for transaction in transactions:
            if transaction.account.account_id in accounts:
                continue
            account = self.core.call_success(
                "db_execute", db,
                "SELECT accounts.rowid FROM accounts"
                " WHERE accounts.id = ?"
                " LIMIT 1",
                (transaction.account.account_id,)
            )
            account = list(account)
            accounts[transaction.account.account_id] = account[0][0]

        self.core.call_success("db_execute", db, "BEGIN TRANSACTION")

        for transaction in transactions:
            if transaction.transaction_id is not None:
                previous = self.core.call_success(
                    "db_execute", db,
                    "SELECT transactions.rowid FROM transactions"
                    " WHERE transactions.id = ? LIMIT 1",
                    (transaction.transaction_id,)
                )
                previous = list(previous)
                if len(previous) > 0:
                    # already in the DB
                    continue
            self.core.call_success(
                "db_execute", db,
                "INSERT INTO transactions ("
                " account, id, label, vdate, amount"
                ") VALUES (?, ?, ?, ?, ?)",
                (
                    accounts[transaction.account.account_id],
                    transaction.transaction_id,
                    transaction.label,
                    int(transaction.vdate.timestamp()),
                    int(transaction.amount * 1000),
                )
            )

        self.core.call_success(
            "db_execute", db,
            "UPDATE transactions SET id = rowid WHERE id IS NULL;"
        )

        self.core.call_success("db_execute", db, "COMMIT")
        db.close()

        self.core.call_all("on_db_modified")

    def db_transaction_upd(self, transaction):
        db = self.core.call_success("db_get_cursor")
        self.core.call_success(
            "db_execute", db,
            "UPDATE transactions SET"
            " label = ?, vdate = ?, amount = ?"
            " WHERE id = ? LIMIT 1",
            (
                transaction.label,
                int(transaction.vdate.timestamp()),
                transaction.amount * 1000,
                transaction.transaction_id,
            )
        )
        self.core.call_success("db_execute", db, "COMMIT")
        db.close()
        self.core.call_all("on_db_modified")

    def db_transaction_del(self, transaction):
        db = self.core.call_success("db_get_cursor")
        self.core.call_success(
            "db_execute", db,
            "DELETE FROM transactions"
            " WHERE id = ? LIMIT 1",
            (transaction.transaction_id,)
        )
        self.core.call_success("db_execute", db, "COMMIT")
        db.close()
        self.core.call_all("on_db_modified")

    def db_transactions_has(self, account, db=None):
        if db is None:
            db = self.core.call_success("db_get_cursor")
        transactions = self.core.call_success(
            "db_execute", db,
            "SELECT COUNT(*) "
            " FROM transactions"
            " INNER JOIN accounts ON accounts.rowid = transactions.account"
            " WHERE accounts.id = ? LIMIT 1",
            (account.account_id,)
        )
        transactions = list(transactions)
        if transactions[0][0] > 0:
            return True
        return None

    def db_transactions_get_balance(self, account, date=None):
        if date is None:
            return account.current_balance
        db = self.core.call_success("db_get_cursor")
        transactions = self.core.call_success(
            "db_execute", db,
            "SELECT transactions.amount "
            " FROM transactions"
            " INNER JOIN accounts ON accounts.rowid = transactions.account"
            " WHERE accounts.id = ?"
            " AND transactions.vdate >= ?"
            " ORDER BY vdate;",
            (account.account_id, date.timestamp())
        )
        balance = account.current_balance
        for t in transactions:
            balance -= (t[0] / 1000)
        return balance
