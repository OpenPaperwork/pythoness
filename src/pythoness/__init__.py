import gettext


def _(s):
    return gettext.dgettext('pythoness', s)
