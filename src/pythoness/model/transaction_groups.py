import datetime
import statistics

import openpaperwork_core
import openpaperwork_core.promise

from . import common


class TransactionGroup(common.Sortable):
    """
    Represent similar transactions. They must share a common denominator
    (see transactions:transaction_get_denominator()).
    Immutable.
    """

    SORT_CRITERIAS = [
        'denominator',
        'start_vdate',
        'end_vdate',
    ]

    def __init__(self, transactions, start_vdate=None, end_vdate=None):
        super().__init__(eq_id=transactions[0].eq_id)
        transactions = sorted(transactions)
        self.transactions = transactions

        self.account = transactions[0].account
        self.denominator = transactions[0].denominator
        self.label = transactions[0].label

        self.start_vdate = start_vdate
        if self.start_vdate is None:
            self.start_vdate = transactions[0].vdate
        self.end_vdate = end_vdate
        if self.end_vdate is None:
            self.end_vdate = transactions[-1].vdate

        amounts = [t.amount for t in transactions]
        self.sum_amount = sum(amounts)
        self.mean_amount = 0
        self.stddev_amount = 0
        self.mean_day_interval = 0
        if len(amounts) > 0:
            self.mean_amount = statistics.mean(amounts)
        if len(amounts) >= 2:
            self.stddev_amount = statistics.stdev(amounts)
        if len(self.transactions) >= 2:
            self.mean_day_interval = statistics.mean(
                self._get_date_intervals(self.transactions)
            )

    def short_mean_amount(self, months=3):
        # Use only the most recent values: Values during the last <months>
        # (3) months. Makes sure there at least 3 values. If not, extend
        # the time period as much as required to get at least 3 values.
        rq_points = 3
        min_date = self.transactions[-1].vdate - datetime.timedelta(
            days=months * 31
        )
        amounts = []
        for t in reversed(self.transactions):
            if rq_points < 0 and t.vdate < min_date:
                # ignore too old values
                break
            amounts.append(t.amount)
        return statistics.mean(amounts)

    @property
    def mean_amount_per_day(self):
        days = (self.end_vdate - self.start_vdate).days
        if days == 0:
            return -1
        return self.sum_amount / days

    @staticmethod
    def _get_date_intervals(transactions):
        for idx in range(0, len(transactions) - 1):
            yield (transactions[idx + 1].vdate - transactions[idx].vdate).days

    def to_dict(self):
        return {
            "mean_amount_per_day": self.mean_amount_per_day,
            "start_vdate": [
                self.start_vdate.year,
                self.start_vdate.month,
                self.start_vdate.day,
            ],
            "end_vdate": [
                self.end_vdate.year,
                self.end_vdate.month,
                self.end_vdate.day,
            ],
            "transactions": [t.eq_id for t in self.transactions]
        }

    @staticmethod
    def from_dict(d, transactions, *args, **kwargs):
        return TransactionGroup(
            transactions=[transactions[t_id] for t_id in d['transactions']],
            start_vdate=datetime.datetime(
                year=d['start_vdate'][0],
                month=d['start_vdate'][1],
                day=d['start_vdate'][2],
            ),
            end_vdate=datetime.datetime(
                year=d['end_vdate'][0],
                month=d['end_vdate'][1],
                day=d['end_vdate'][2],
            ),
        )

    def __str__(self):
        return (
            'TransactionGroup('
            'denominator={}, '
            'len(transactions)={}, '
            'mean_amount={:.2f}, '
            'short_mean_amount={:.2f}, '
            'mean_day_interval={:.2f}, '
            'label={}'
            ')'
        ).format(
            self.denominator,
            len(self.transactions),
            self.mean_amount,
            self.short_mean_amount(months=3),
            self.mean_day_interval,
            self.label
        )


class Plugin(openpaperwork_core.PluginBase):
    def get_interfaces(self):
        return [
            'transaction_groups',
        ]

    def transaction_group_new(
            self, transactions, start_vdate=None, end_vdate=None):
        return TransactionGroup(transactions, start_vdate, end_vdate)

    def transaction_group_all(self, out: list, all_transactions):
        groups = {}
        for transaction in all_transactions:
            tg_id = (transaction.account, transaction.denominator)
            if tg_id in groups:
                groups[tg_id].append(transaction)
            else:
                groups[tg_id] = [transaction]
        r = [
            TransactionGroup(transactions) for transactions in groups.values()
            if len(transactions) > 1
        ]
        r.sort()
        out += r
        return True

    def transaction_group_all_promise(self, all_transactions):
        out = []
        promise = openpaperwork_core.promise.ThreadedPromise(
            self.core, self.transaction_group_all,
            args=(out, all_transactions)
        )
        promise = promise.then(lambda *args, **kwargs: out)
        return promise
