import collections
import logging

import openpaperwork_core

from ... import _


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = 1000

    def __init__(self):
        super().__init__()
        self.widget_tree = None
        self.accounts = {}

    def get_interfaces(self):
        return [
            'gtk_account_selector',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'account_storage',
                'defaults': ['pythoness.sqlite.accounts'],
            },
            {
                'interface': 'gtk_dialog_yes_no',
                'defaults': ['openpaperwork_gtk.dialogs.yes_no'],
            },
            {
                'interface': 'gtk_dialog_single_entry',
                'defaults': ['openpaperwork_gtk.dialogs.single_entry'],
            },
            {
                'interface': 'gtk_mainwindow',
                'defaults': ['pythoness.mainwindow'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
        ]

    def init(self, core):
        super().init(core)

        self.widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.accounts", "accounts.glade"
        )
        if self.widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return
        accounts = self.widget_tree.get_object("account_list")
        accounts.clear()

        self.on_db_modified()

        self.widget_tree.get_object("account_add").connect(
            "clicked", self._on_add
        )
        self.widget_tree.get_object("account_edit").connect(
            "clicked", self._on_edit
        )
        self.widget_tree.get_object("account_remove").connect(
            "clicked", self._on_remove
        )

        list_view = self.widget_tree.get_object("account_list_view")
        list_view.connect("row-activated", self._on_toggle)

        toggle = self.widget_tree.get_object("account_toggle")
        toggle.connect("clicked", self._on_toggle)

        widget = self.widget_tree.get_object("account_selector")
        self.core.call_all("mainwindow_add_left", widget)

    def _on_add(self, button):
        self.core.call_all(
            "gtk_show_dialog_single_entry",
            self, _("New account"), "", None
        )

    def _on_edit(self, button):
        list_view = self.widget_tree.get_object("account_list_view")
        (model, rows) = list_view.get_selection().get_selected_rows()
        row = model[rows[0]]
        account = self.core.call_success("db_account_get", row[0])
        self.core.call_all(
            "gtk_show_dialog_single_entry",
            self, _("Rename account"), account.label, row[0]
        )

    def on_dialog_single_entry_reply(
            self, parent, reply, new_account_label, *args, **kwargs):
        if parent is not self:
            return
        if not reply:
            return
        (account_id,) = args

        if account_id is not None:
            LOGGER.info(
                "Updating account label: %s: %s", account_id, new_account_label
            )
            account = self.core.call_success("db_account_get", account_id)
            account.label = new_account_label
            self.core.call_all("db_account_update", account)
        else:
            LOGGER.info("Creating account %s", new_account_label)
            account = self.core.call_success(
                "account_new", new_account_label, new_account_label, 0.0
            )
            self.core.call_all("db_account_add", account)

    def _on_remove(self, button):
        list_view = self.widget_tree.get_object("account_list_view")
        (model, rows) = list_view.get_selection().get_selected_rows()
        account_ids = [str(model[row][0]) for row in rows]
        self.core.call_all(
            "gtk_show_dialog_yes_no", self, _("Are you sure ?"),
            account_ids
        )

    def on_dialog_yes_no_reply(self, parent, reply, *args, **kwargs):
        if parent is not self:
            return
        if not reply:
            return
        (account_ids,) = args
        for account_id in account_ids:
            LOGGER.info("Deleting account %s", account_id)
            account = self.core.call_success("db_account_get", account_id)
            self.core.call_all("db_account_delete", account)

    def on_db_modified(self):
        self.core.call_success("mainloop_schedule", self._on_db_modified)

    def _on_db_modified(self):
        account_list = self.widget_tree.get_object("account_list")

        states = collections.defaultdict(lambda: False)
        for (a_id, label, disabled, color) in account_list:
            states[a_id] = disabled

        account_list.clear()

        accounts = []
        self.core.call_success("db_account_get_all", accounts)
        accounts.sort(key=lambda a: a.label.lower())

        self.accounts = {}
        for account in accounts:
            self.accounts[account.account_id] = account
            backend = account.account_id.split("@", 1)
            if len(backend) <= 1:
                backend = ""
            else:
                backend = " ({})".format(backend[1])
            account_list.append((
                account.account_id,
                account.label + backend,
                states[account.account_id],
                "black" if not states[account.account_id] else "gray"
            ))

        self._notify_visibles()

    def _on_toggle(self, *args, **kwargs):
        list_view = self.widget_tree.get_object("account_list_view")
        (model, rows) = list_view.get_selection().get_selected_rows()
        for row in rows:
            row = model[row]
            row[2] = not row[2]
            row[3] = "black" if not row[2] else "gray"
        self._notify_visibles()

    def _notify_visibles(self):
        account_list = self.widget_tree.get_object("account_list")
        accounts = set()
        for (a_id, label, disabled, color) in account_list:
            if disabled:
                continue
            accounts.add(self.accounts[a_id])
        LOGGER.info("Visible accounts: %s", accounts)
        self.core.call_all("set_visible_accounts", accounts)
