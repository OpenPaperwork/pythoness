import logging

import openpaperwork_core


LOGGER = logging.getLogger(__name__)


class Plugin(openpaperwork_core.PluginBase):
    PRIORITY = -1000

    def get_interfaces(self):
        return [
            'gtk_menu_item_analyze',
        ]

    def get_deps(self):
        return [
            {
                'interface': 'analyze',
                'defaults': ['pythoness.model.analyze'],
            },
            {
                'interface': 'gtk_mainwindow',
                'defaults': ['pythoness.gui.mainwindow'],
            },
            {
                'interface': 'gtk_resources',
                'defaults': ['openpaperwork_gtk.resources'],
            },
        ]

    def on_initialized(self):
        widget_tree = self.core.call_success(
            "gtk_load_widget_tree",
            "pythoness.gui.analyze", "analyze.glade"
        )
        if widget_tree is None:
            # init must still work so 'chkdeps' is still available
            return

        menu_item = widget_tree.get_object("menu_item_analyze")
        menu_item.connect("activate", self._on_analyze, widget_tree)
        self.core.call_all("app_menu_add", menu_item)

    def _on_analyze(self, menu_item, widget_tree):
        self.core.call_all("analyze")
