# Pythoness

## Status

Proof-of-concept


## Description

Pythoness is (will be) a predictive personal finance manager. It is designed to
do the work for you as much as possible.

Pythoness can:
* download automatically your bank statements using
  [Weboob](http://weboob.org/).
* guess what transactions are recurrent.
* average the remaining spendings and earnings.
* use its guesses to predict the future :-)

Things to know:
- To figure out if a transaction is recurrent, Pythoness
  needs at least 3 actual transactions (2 to make an hypothesis +
  1 to confirm the hypothesis). Many banks provide online statements
  only for the last 2 months. In other words, for a monthly transaction,
  they only provide the last 2 transactions. So you will need to collect
  online statements (and merge them) long enough for Pythoness to work
  correctly.
- Weboob does not support 2-factor authentication.
- Pythoness can hardly predict correctly transfers between your own
  accounts. When reading predictions, you may want to focus on
  the predicted total for all the accounts instead of individual
  account balance.
- Non-recurrent transactions are averaged to guess how much
  you earn and spend on average each month. Only the last 3 months are
  taken into account.
- Passwords are not stored at all.


## Installation

```sh
sudo apt install python3-pip python3-virtualenv virtualenv

mkdir ~/git
cd ~/git
git clone https://framagit.org/jflesch/Pythoness.git pythoness
cd pythoness

virtualenv -p python3 venv
source venv/bin/activate

python3 ./setup.py install
```


## Update

```sh
cd ~/git/pythoness
git pull
source venv/bin/activate
python3 ./setup.py install
```


## Configuration

You just need to install and enable the Weboob/Boobank backends you need.
A script is provided to do that quickly.

```sh
cd ~/git/pythoness
source venv/bin/activate
poc/wb_install_backend.py
```


## Downloading bank statements

```sh
cd ~/git/pythoness
source venv/bin/activate
# poc/dump.py <weboob bank backend> <login> <output file>
# will ask for password (won't be shown)
poc/dump.py fortuneo 111222333 fortuneo.json
poc/dump.py creditmutuel 111222333 creditmutuel.json
```


## Merging downloaded bank statements

Can be used to merge statements from many banks or to merge
statements downloaded at various points in time.

```sh
cd ~/git/pythoness
source venv/bin/activate
# poc/merge.my <input file 1> <input file 2> <output file>
poc/merge.py fortuneo.json creditmutuel.json merged.json
```


## Analysis

Every time you download or update a statement, an analysis must be run
on them. The analysis will look for recurrent transactions.
It may take a few minutes.

```sh
cd ~/git/pythoness
source venv/bin/activate
# poc/analyze.py <input file> <output file>
poc/analyze.py merged.json analysis.json
```


## Seeing analysis results

```sh
cd ~/git/pythoness
source venv/bin/activate
# poc/show.py <input file>
poc/show.py analysis.json
```


## Predicting the future

```
cd ~/git/pythoness
source venv/bin/activate
# poc/predict.py <input file> <target date>
# Date format is: YYYY-MM-DD
poc/predict.py analysis.json 2019-12-01
```
