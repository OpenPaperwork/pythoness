#!/usr/bin/env python3

from setuptools import setup

setup(
    name="pythoness",
    version="0.1",
    description=("Predictive personal finance manager"),
    keywords="personal finance predictive",
    url="https://framagit.org/jflesch/pythoness",
    classifiers=[
        "Development Status :: 1 - Planning",
        "Intended Audience :: End USers/Desktop",
        "License :: OSI Approved :: GNU General Public License v3 or later"
        " (GPLv3+)",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.5",
        "Topic :: Office/Business :: Financial :: Accounting",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "Topic :: Scientific/Engineering :: Information Analysis"
    ],
    license="GPLv3+",
    author="Jerome Flesch",
    author_email="jflesch@openpaper.work",
    packages=[
        'pythoness',
        'pythoness.model',
        'pythoness.gui',
    ],
    include_package_data=True,
    data_files=[],
    scripts=[],
    zip_safe=True,
    install_requires=[
        "weboob",
    ],
    entry_points={
        'gui_scripts': [
            'pythoness = pythoness.gui:main',
        ]
    }
)
