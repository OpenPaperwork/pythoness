#!/usr/bin/env python3

"""
Transactions can be seen as 2D points: one axis is the amount of the
transaction and another axis is its date. Here we are interested in comparing
how close predicted transactions are to other actual transactions. Since we
have 2 axes, we can compute a distance. But since don't want to favor date or
amounts, we have to bring them back to the same scale: [0.0-1.0]. Therefore
the distances will generally be between [0.0-1.0] (more than 1.0 for really
inaccurate predictions, but since they are inaccurate, they will quickly be
ignored) (see Transaction.distance()).

Score for *one* prediction is between [0.0-1.0]. It is 1-d,
d = distance between the prediction and the actual closest transaction.

Score for a prediction rule is between [0.0-n] (n = number of matching
elements with a score > MAGIC_MIN_SCORE).

While there are transactions and possible valid predictions,
we keep the best prediction. For each best predictions, we remove
the correctly predicted transactions from the original set, and
restart.
"""

import datetime

from . import transaction


MAGIC_SHORT_MEAN_MONTHS = 3


# MAGIC(JFlesch): define the minimum score at which we consider the
# prediction to be possibly valid. Anything with a lower score must be ignored
MAGIC_MIN_SCORE = 0.75

MAGIC_END_MARGIN = 0.7

MAGIC_COMMON_DAY_INTERVALS = [
    # day intervals that are actually common with recurrent payments
    (1, 3),
    (7, 8),
    (14, 15),
    (29, 32),
    (60, 63),
    (90, 93),
    (120, 124),
    (180, 187),
    (362, 368),  # some margin for error
]


class PredictionException(Exception):
    pass


class PredictionRule(transaction.TransactionGroup):
    """
    Prediction rules are based on 2 transactions. With those 2 transactions
    (let call them tA and tB), we can try to predict a future transaction (tP):
    tP.amount = average(tA.amount, tB.amount)
    tP.date = tB.date + (tB.date - tA.date)

    We can then try to see if the predicted transaction actually exists.

    If it does, we can include the newly predicted transaction:
    tP.amount = mean(all_predicted)
    tP.date = tLastPredicted + average(interval_dates)
    """

    def __init__(self, transactions,
                 has_ended=None, end_date=datetime.datetime.now()):
        assert(len(transactions) >= 2)

        self.facts = transactions[:]
        self.facts.sort()  # sorted by dates
        super().__init__(self.facts)

        self.label = self.facts[-1].label
        self.has_ended = has_ended
        if self.has_ended is None:
            self.has_ended = self._has_ended(end_date)

    def _has_ended(self, end_date):
        return (
            self.facts[-1].vdate +
            datetime.timedelta(days=self.mean_day_interval) +
            datetime.timedelta(days=(
                self.mean_day_interval * MAGIC_END_MARGIN + 1
            )) < end_date
        )

    @staticmethod
    def from_dict(d, transactions, *args, **kwargs):
        return PredictionRule(
            transactions=[
                transactions[tuple(t_id)] for t_id in d['transactions']
            ],
            has_ended=d['has_ended'],
        )

    def to_dict(self):
        p = super().to_dict()
        p['has_ended'] = self.has_ended
        return p

    def __str__(self):
        return (
            "PredictionRule(first={}, last={}, predicted={},"
            " amount={:.2f} (stdev={:.2f}), interval={:.2f}, has_ended={},"
            " score={}"
            ")".format(
                self.facts[0].vdate.date(),
                self.facts[-1].vdate.date(),
                len(self.facts),
                self.short_mean_amount(months=MAGIC_SHORT_MEAN_MONTHS),
                self.stddev_amount,
                self.mean_day_interval,
                self.has_ended,
                self.score(),
            )
        )

    def __repr__(self):
        return str(self)

    @staticmethod
    def gen_id(transactions):
        h = 0
        for t in transactions:
            h ^= hash(t.eq_id)
        return str(h)

    @staticmethod
    def _days_range(start, end, interval):
        """
        Cannot use range() because interval is a float
        """
        while start <= end:
            yield start
            start += interval

    def predict(self, start_date, end_date):
        if self.has_ended:
            return

        last = self.facts[-1]
        if end_date <= last.vdate:
            raise PredictionException("Can't predict events before the facts")

        start_diff = (start_date - last.vdate).days
        if start_diff < 1:
            start_diff = 1
        end_diff = (end_date - last.vdate).days

        if (start_diff % self.mean_day_interval) != 0:
            start_diff += self.mean_day_interval
        start_diff = start_diff - (start_diff % self.mean_day_interval)

        for (idx, diff) in enumerate(
                    self._days_range(
                        start_diff, end_diff + 1,
                        self.mean_day_interval
                    )
                ):
            yield transaction.Transaction(
                account=self.facts[0].account,
                transaction_id="{}-predicted-{}".format(self.eq_id, idx),
                label="{} (predicted)".format(self.label),
                vdate=last.vdate + datetime.timedelta(days=diff),
                amount=self.short_mean_amount(months=MAGIC_SHORT_MEAN_MONTHS),
            )

    def is_valid(self):
        # see if the interval is considered valid
        if MAGIC_COMMON_DAY_INTERVALS is not None:
            interval = self.mean_day_interval
            for (low, high) in MAGIC_COMMON_DAY_INTERVALS:
                if low <= interval and interval <= high:
                    break
            else:
                return False

        # if the standard deviation becomes too important
        # this prediction probably makes no sense
        min_amount = min([abs(t.amount) for t in self.facts])
        max_amount = max([abs(t.amount) for t in self.facts])
        if min_amount == 0:
            return True
        return (
            ((max_amount - min_amount) / min_amount) <=
            (1.0 - MAGIC_MIN_SCORE)
        )

    @staticmethod
    def find_closest(rule, all_transactions, prediction):
        # TODO(Jflesch): Could probably be optimized ...
        all_distances = [
            (
                prediction.distance(
                    transaction,
                    scale_date_interval=rule.mean_day_interval,
                    scale_amount=rule.short_mean_amount(
                        months=MAGIC_SHORT_MEAN_MONTHS
                    ),
                ),
                transaction
            )
            for transaction in all_transactions
        ]
        closest = min(all_distances)
        return closest

    def expand(self, all_transactions, end_date=datetime.datetime.now()):
        """
        Based on the initial transactions, go through the given transactions
        and include those that could match this prediction rule.
        Return a new prediction rules including all the transactions
        that could match.
        """
        if self.has_ended:
            return self

        current_rule = self
        while True:
            # we only care about future transactions
            future = all_transactions[
                all_transactions.index(current_rule.facts[-1]) + 1:
            ]
            if len(future) <= 0:
                break

            # let's try to predict the future
            prediction = transaction.Transaction(
                account=self.facts[0].account,
                transaction_id="must-not-be-used-anywhere-else",
                label=current_rule.facts[-1].label,
                vdate=(
                    current_rule.facts[-1].vdate +
                    datetime.timedelta(days=current_rule.mean_day_interval)
                ),
                amount=current_rule.short_mean_amount(
                    months=MAGIC_SHORT_MEAN_MONTHS
                )
            )

            # and let see how good we are (or not)
            (distance, actual) = self.find_closest(
                current_rule, future, prediction
            )
            score = 1.0 - distance
            if score < MAGIC_MIN_SCORE:
                # no more valid predictions
                current_rule = PredictionRule(
                    transactions=current_rule.facts,
                    has_ended=False
                )
                break

            new_rule = PredictionRule(
                transactions=current_rule.facts + [actual],
                has_ended=False,
            )
            if not new_rule.is_valid():
                break
            current_rule = new_rule

        # take into account end_date to see if the rule is now
        # obsolete or not
        current_rule = PredictionRule(
            transactions=current_rule.facts,
            end_date=end_date,
        )
        return current_rule

    def score(self):
        score = 0.0
        for idx in range(1, len(self.facts) - 1):
            current_t = self.facts[idx]
            next_t = self.facts[idx + 1]
            # let's try to predict the next one
            prediction = transaction.Transaction(
                account=self.facts[0].account,
                transaction_id="must-not-be-used-anywhere-else",
                label=current_t.label,
                vdate=(
                    current_t.vdate +
                    datetime.timedelta(days=self.mean_day_interval)
                ),
                amount=self.short_mean_amount(months=MAGIC_SHORT_MEAN_MONTHS)
            )
            score += 1.0 - (
                prediction.distance(
                    next_t,
                    scale_date_interval=self.mean_day_interval,
                    scale_amount=self.short_mean_amount(
                        months=MAGIC_SHORT_MEAN_MONTHS
                    ),
                )
            )
        return score

    @staticmethod
    def _gen_all_predictions(all_transactions, end_date):
        for (t_a_idx, t_a) in enumerate(all_transactions):
            for (t_b_idx, t_b) in enumerate(
                        all_transactions[t_a_idx + 1:]
                    ):
                if t_a.vdate.date() == t_b.vdate.date():
                    # no recurrence possible
                    continue
                rule = PredictionRule([t_a, t_b], has_ended=False)
                if not rule.is_valid():
                    continue
                rule = rule.expand(
                    all_transactions[t_a_idx + t_b_idx + 1:],
                    end_date=end_date,
                )
                if rule.has_ended:
                    # ignore ended predictions. Better include them
                    # in averages if pertinent
                    continue
                yield (rule.score(), rule)

    @staticmethod
    def guess_all_rules(all_transactions, end_date=datetime.datetime.now()):
        """
        Try to figure out all the best prediction rules.
        """
        all_transactions = all_transactions[:]
        all_transactions.sort()

        out = []
        while len(all_transactions) > 2:
            # find the best possible rule
            best = None
            for rule in PredictionRule._gen_all_predictions(
                        all_transactions, end_date
                    ):
                best = max(best, rule) if best is not None else rule
            if best is None:
                break
            best = best[1]
            if len(best.facts) <= 2:
                break
            out.append(best)

            # remove correctly predicted transactions, and try again
            for t in best.facts:
                all_transactions.remove(t)
        out.sort()
        return out
