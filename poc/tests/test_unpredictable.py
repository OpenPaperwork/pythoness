#!/usr/bin/env python3

import datetime
import unittest

from pythoness.model import transaction
from pythoness.model import unpredictable


generated = 0


def gen_id():
    global generated
    generated += 1
    return str(generated)


DUMB_ACCOUNT = transaction.Account("dumb_account", "dumb_account")


class UnpredictableRuleTestCase(unittest.TestCase):
    def setUp(self):
        unpredictable.MAGIC_MAX_LOOPBACK = 3 * 31

    def test_get_unpredictable_bad_input(self):
        out = unpredictable.UnpredictableRule.get_unpredictable_rules([], [])
        self.assertEqual(out, {})

        transactions = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=3, day=1),
                amount=-30.0,
            ),
        ]
        out = unpredictable.UnpredictableRule.get_unpredictable_rules(
            transactions, predictions=[],
            end_date=datetime.datetime(year=2018, month=3, day=1)
        )
        self.assertEqual(out, {})

    def test_get_unpredictable_simple(self):
        transactions = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=3),
                amount=-30.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=18),
                amount=-30.0,
            ),
        ]
        out = unpredictable.UnpredictableRule.get_unpredictable_rules(
            transactions, predictions=[],
            end_date=datetime.datetime(year=2018, month=3, day=1)
        )
        self.assertEqual(len(out), 1)
        out = out[DUMB_ACCOUNT]
        self.assertEqual(round(out.mean_amount_per_day, 2), -2.31)

    def test_get_unpredictable_strip_old(self):
        transactions = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=3),
                amount=-30.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2016, month=2, day=3),
                amount=-30.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=18),
                amount=-30.0,
            ),
        ]
        out = unpredictable.UnpredictableRule.get_unpredictable_rules(
            transactions, predictions=[],
            end_date=datetime.datetime(year=2018, month=3, day=1)
        )
        self.assertEqual(len(out), 1)
        out = out[DUMB_ACCOUNT]
        self.assertEqual(round(out.mean_amount_per_day, 2), -2.31)

    def test_get_unpredictable(self):
        transactions = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=3),
                amount=-30.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="SPOTIFY FRANCE 75 PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-40.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="AMAZON PAYMENTS PARIS9999999/",
                vdate=datetime.datetime(year=2018, month=2, day=1),
                amount=-70.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="AMAZON PAYMENTS PARIS9999999/",
                vdate=datetime.datetime(year=2018, month=2, day=10),
                amount=-90.0,
            ),
        ]
        out = unpredictable.UnpredictableRule.get_unpredictable_rules(
            transactions, predictions=[],
            end_date=datetime.datetime(year=2018, month=2, day=10)
        )
        self.assertEqual(len(out), 1)
        out = out[DUMB_ACCOUNT]
        self.assertEqual(round(out.mean_amount_per_day, 2), -5.75)
