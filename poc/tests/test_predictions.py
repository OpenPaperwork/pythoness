#!/usr/bin/env python3

import datetime
import unittest

from pythoness.model import prediction
from pythoness.model import transaction


generated = 0


def gen_id():
    global generated
    generated += 1
    return str(generated)


DUMB_ACCOUNT = transaction.Account("dumb_account", "dumb_account")


class PredictionRuleInstTestCase(unittest.TestCase):
    def setUp(self):
        prediction.MAGIC_MIN_SCORE = 0.75
        prediction.MAGIC_END_MARGIN = 0.7
        prediction.MAGIC_COMMON_DAY_INTERVALS = None

    def testSimpleBasePrediction(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
        ]
        rule = prediction.PredictionRule(all_t, has_ended=False)
        self.assertEqual(rule.mean_day_interval, 14)
        self.assertEqual(rule.mean_amount, -10.0)
        self.assertEqual(rule.score(), 0)

    def testSimplePerfectPrediction(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=29),
                amount=-10.0,
            ),
        ]
        rule = prediction.PredictionRule(all_t, has_ended=False)
        self.assertEqual(rule.mean_day_interval, 14)
        self.assertEqual(rule.mean_amount, -10.0)
        self.assertEqual(rule.score(), 1.0)

        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=29),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=12),
                amount=-10.0,
            ),
        ]
        rule = prediction.PredictionRule(all_t, has_ended=False)
        self.assertEqual(rule.mean_day_interval, 14)
        self.assertEqual(rule.mean_amount, -10.0)
        self.assertEqual(rule.score(), 2.0)

    def testSimpleBadPrediction(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=12, day=29),
                amount=-10.0,
            ),
        ]
        rule = prediction.PredictionRule(all_t, has_ended=False)
        self.assertEqual(rule.mean_day_interval, 181)
        self.assertEqual(rule.mean_amount, -10.0)
        self.assertEqual(round(rule.score(), 3), 0.077)

        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=29),
                amount=-200.0,
            ),
        ]
        rule = prediction.PredictionRule(all_t, has_ended=False)
        self.assertEqual(rule.mean_day_interval, 14)
        self.assertEqual(round(rule.mean_amount, 2), -73.33)
        # so bad score is even negative :-)
        self.assertEqual(round(rule.score(), 3), -0.727)

    def testSimplePerfectExpand(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=29),
                amount=-10.0,
            ),
        ]
        base_rule = prediction.PredictionRule(all_t[:2], has_ended=False)
        expanded_rule = base_rule.expand(all_t)
        self.assertEqual(len(expanded_rule.facts), 3)
        self.assertEqual(expanded_rule.mean_day_interval, 14)
        self.assertEqual(expanded_rule.mean_amount, -10.0)
        self.assertEqual(expanded_rule.score(), 1.0)

    def testSimpleExpand(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=28),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=11),
                amount=-9,
            ),
        ]
        base_rule = prediction.PredictionRule(all_t[:2], has_ended=False)
        expanded_rule = base_rule.expand(all_t)
        self.assertEqual(len(expanded_rule.facts), 4)
        self.assertEqual(round(expanded_rule.mean_day_interval, 2), 13.67)
        self.assertEqual(round(expanded_rule.mean_amount, 2), -9.75)
        self.assertEqual(round(expanded_rule.score(), 2), 1.87)

    def testSimpleBadExpand(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2019, month=1, day=29),
                amount=-10.0,
            ),
        ]
        base_rule = prediction.PredictionRule(all_t[:2], has_ended=False)
        expanded_rule = base_rule.expand(all_t)
        self.assertEqual(len(expanded_rule.facts), 2)
        self.assertEqual(expanded_rule.mean_day_interval, 14)
        self.assertEqual(expanded_rule.mean_amount, -10.0)
        self.assertEqual(expanded_rule.score(), 0.0)

        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=29),
                amount=-200.0,
            ),
        ]
        base_rule = prediction.PredictionRule(all_t[:2], has_ended=False)
        expanded_rule = base_rule.expand(all_t)
        self.assertEqual(len(expanded_rule.facts), 2)
        self.assertEqual(expanded_rule.mean_day_interval, 14)
        self.assertEqual(expanded_rule.mean_amount, -10.0)
        self.assertEqual(expanded_rule.score(), 0.0)

    def testMessExpand(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2017, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(  # initial
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=17),
                amount=-10.0,
            ),
            transaction.Transaction(  # initial
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=21),
                amount=-200.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=29),
                amount=-99.0,
            ),
            transaction.Transaction(  # to find
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=29),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=5),
                amount=-99.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=13),
                amount=-99.0,
            ),
            transaction.Transaction(  # to find
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=2, day=10),
                amount=-9.5,
            ),
        ]
        base_rule = prediction.PredictionRule(
            [all_t[1], all_t[3]], has_ended=False
        )
        expanded_rule = base_rule.expand(all_t)
        self.assertEqual(
            expanded_rule.facts,
            [all_t[1], all_t[3], all_t[6], all_t[9]]
        )
        self.assertEqual(round(expanded_rule.mean_day_interval, 2), 13.33)
        self.assertEqual(expanded_rule.mean_amount, -9.875)
        self.assertEqual(round(expanded_rule.score(), 2), 1.84)


class GuessAllPredictionsTestCase(unittest.TestCase):
    def setUp(self):
        prediction.MAGIC_MIN_SCORE = 0.75
        prediction.MAGIC_END_MARGIN = 0.7
        prediction.MAGIC_COMMON_DAY_INTERVALS = None

    def testTooSmall(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=2),
                amount=-10.0,
            ),
        ]
        self.assertEqual(
            list(prediction.PredictionRule.guess_all_rules(
                all_t, end_date=datetime.datetime(year=2018, month=1, day=3),
            )),
            []
        )

        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=2),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=3),
                amount=-10.0,
            ),
        ]
        self.assertEqual(
            list(prediction.PredictionRule.guess_all_rules(
                all_t, end_date=datetime.datetime(year=2018, month=1, day=4)
            )),
            []
        )

    def testSimplePredictions(self):
        all_t = [
            transaction.Transaction(  # P1
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=2),
                amount=-10.0,
            ),
            transaction.Transaction(  # P2
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="PAYPAL 9999 999 999",
                vdate=datetime.datetime(year=2018, month=1, day=5),
                amount=-95.0,
            ),
            transaction.Transaction(  # P1
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=3),
                amount=-10.0,
            ),
            transaction.Transaction(  # P2
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="PAYPAL 9999 999 999",
                vdate=datetime.datetime(year=2018, month=1, day=10),
                amount=-94.0,
            ),
            transaction.Transaction(  # P1
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=4),
                amount=-10.1,
            ),
            transaction.Transaction(  # P2
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="PAYPAL 9999 999 999",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-95.0,
            ),
        ]
        all_p = list(prediction.PredictionRule.guess_all_rules(
            all_t, end_date=datetime.datetime(year=2018, month=1, day=16)
        ))
        self.assertEqual(all_p[0].facts, [all_t[1], all_t[3], all_t[5]])
        self.assertEqual(all_p[1].facts, [all_t[0], all_t[2], all_t[4]])

    def testWithJunk(self):
        self.maxDiff = None

        all_t = [
            transaction.Transaction(  # P1 : 0
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=2),
                amount=-10.0,
            ),
            transaction.Transaction(  # P2 : 1
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="PAYPAL 9999 999 999",
                vdate=datetime.datetime(year=2018, month=1, day=5),
                amount=-95.0,
            ),
            transaction.Transaction(  # 2
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="JUNK 3541",
                vdate=datetime.datetime(year=2018, month=1, day=5),
                amount=-75.0,
            ),
            transaction.Transaction(  # 3
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="JUNK 3541",
                vdate=datetime.datetime(year=2018, month=1, day=5),
                amount=-235.0,
            ),
            transaction.Transaction(  # 4
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="JUNK 3541",
                vdate=datetime.datetime(year=2018, month=1, day=8),
                amount=-53.0,
            ),
            transaction.Transaction(  # P2 : 5
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="PAYPAL 9999 999 999",
                vdate=datetime.datetime(year=2018, month=1, day=10),
                amount=-95.0,
            ),
            transaction.Transaction(  # P1 : 6
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=12),
                amount=-10.0,
            ),
            transaction.Transaction(  # P2 : 7
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="PAYPAL 9999 999 999",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-95.0,
            ),
            transaction.Transaction(  # P1 : 8
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=22),
                amount=-10.0,
            ),
        ]
        all_p = list(prediction.PredictionRule.guess_all_rules(
            all_t, end_date=datetime.datetime(year=2018, month=1, day=23)
        ))
        self.assertEqual(len(all_p[0].facts), 3)
        self.assertEqual(len(all_p[1].facts), 3)
        self.assertEqual(all_p[0].facts, [all_t[1], all_t[5], all_t[7]])
        self.assertEqual(all_p[1].facts, [all_t[0], all_t[6], all_t[8]])


class PredictionTestCase(unittest.TestCase):
    def setUp(self):
        prediction.MAGIC_MIN_SCORE = 0.75
        prediction.MAGIC_END_MARGIN = 0.7
        prediction.MAGIC_COMMON_DAY_INTERVALS = None

    def testSimpleBasePrediction(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
        ]
        rule = prediction.PredictionRule(all_t, has_ended=False)
        predictions = list(rule.predict(
            datetime.datetime(year=2018, month=1, day=16),
            datetime.datetime(year=2018, month=3, day=30),
        ))
        self.assertEqual(predictions[0].amount, -10.0)
        self.assertEqual(
            predictions[0].vdate,
            datetime.datetime(year=2018, month=1, day=29)
        )
        self.assertEqual(predictions[1].amount, -10.0)
        self.assertEqual(
            predictions[1].vdate,
            datetime.datetime(year=2018, month=2, day=12)
        )
        self.assertEqual(predictions[2].amount, -10.0)
        self.assertEqual(
            predictions[2].vdate,
            datetime.datetime(year=2018, month=2, day=26)
        )

    def testFurtherPrediction(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
        ]
        rule = prediction.PredictionRule(all_t, has_ended=False)
        predictions = list(rule.predict(
            datetime.datetime(year=2018, month=5, day=1),
            datetime.datetime(year=2018, month=5, day=30),
        ))
        self.assertEqual(predictions[0].amount, -10.0)
        self.assertEqual(
            predictions[0].vdate,
            datetime.datetime(year=2018, month=5, day=7)
        )
        self.assertEqual(predictions[1].amount, -10.0)
        self.assertEqual(
            predictions[1].vdate,
            datetime.datetime(year=2018, month=5, day=21)
        )

    def testDatesIncluded(self):
        all_t = [
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify PARIS",
                vdate=datetime.datetime(year=2018, month=1, day=15),
                amount=-10.0,
            ),
            transaction.Transaction(
                account=DUMB_ACCOUNT,
                transaction_id=gen_id(),
                label="Spotify P9999999 Stockholm",
                vdate=datetime.datetime(year=2018, month=1, day=1),
                amount=-10.0,
            ),
        ]
        rule = prediction.PredictionRule(all_t, has_ended=False)
        predictions = list(rule.predict(
            datetime.datetime(year=2018, month=5, day=7),
            datetime.datetime(year=2018, month=5, day=21),
        ))
        self.assertEqual(predictions[0].amount, -10.0)
        self.assertEqual(
            predictions[0].vdate,
            datetime.datetime(year=2018, month=5, day=7)
        )
        self.assertEqual(predictions[1].amount, -10.0)
        self.assertEqual(
            predictions[1].vdate,
            datetime.datetime(year=2018, month=5, day=21)
        )
